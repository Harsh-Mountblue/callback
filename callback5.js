const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

function callback() {
    setTimeout(() => {
        boardId = "mcu453ed";
        callback1(boardId, (err, data) => {
            print(err, data)

            callback2(boardId, (err, data) => {
                print(err, data)

                let result = data.filter((element) => element.name === "Mind" || element.name === "Space");
                result.forEach(element => {
                    callback3(element.id, (err, data) => {
                        print(err, data)
                    }); 
                });
            });
        });
    }, 2 * 1000);

    function print(err, data) {
        if (err) {
            console.error("Error occured");
            console.error(err);
        } else {
            console.log(data);
        }
    }
}

module.exports = callback;