const fs = require("fs");

function callback(listId, cb) {
    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("passed callback is not a function");
        } else if (typeof listId !== "string") {
            cb(new Error("passed id is not correct"));
        } else {
            try {
                fs.readFile("./data/cards.json", (err, data) => {
                    if (err) {
                        cb(err);
                    } else {
                        let cardData = JSON.parse(data);
                        if (cardData[listId]) {
                            cb(null, cardData[listId]);
                        } else {
                            let customErr = new Error(" id is not present in data");
                            cb(customErr);
                        }
                    }
                });
            } catch (error) {
                cb(error);
            }
        }
    }, 2 * 1000);
}

module.exports = callback;
