const fs = require("fs");

function callback(boardId, cb) {
    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("passed callback is not a function");
        } else if (typeof boardId !== "string") {
            cb(new Error("passed id is not correct"));
        } else {
            try {
                fs.readFile("./data/boards.json", (err, data) => {
                    if (err) {
                        cb(err);
                    } else {
                        let boardData = JSON.parse(data);
                        let result = boardData.filter(
                            (element) => element.id === boardId
                        );

                        if (result.length) {
                            cb(null, result);
                        } else {
                            cb(new Error("id not found in data"));
                        }
                    }
                });
            } catch (error) {
                cb(error);
            }
        }
    }, 2000);
}

module.exports = callback;
