const fs = require("fs");

function callback(boardId, cb) {
    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("passed callback is not a function");
        } else if (typeof boardId !== "string") {
            cb(new Error("passed id is not correct"));
        } else {
            try {
                fs.readFile("./data/lists.json", (err, data) => {
                    if (err) {
                        cb(err);
                    } else {
                        let listData = JSON.parse(data);
                        if (listData[boardId]) {
                            cb(null, listData[boardId]);
                        } else {
                            cb(new Error(" id is not present in data"));
                        }
                    }
                });
            } catch (error) {
                cb(error);
            }
        }
    }, 2000);
}

module.exports = callback;
